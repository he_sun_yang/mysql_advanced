#  **MySQL高级** 

#### 介绍
MySQL深度总结，4天掌握MySQL高阶版！！！

MySQL连接
jdbc:mysql://localhost:3306/demo?useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8&zeroDateTimeBehavior=convertToNull

#### 使用说明

>     day01. MySQL 高级 - Linux上安装MySQL
>     day01. MySQL 高级 - 启动及登录MySQL
>     day01. MySQL 高级 - 索引 - 概述
>     day01. MySQL 高级 - 索引 - 优势和劣势
>     day01. MySQL 高级 - 索引 - 数据结构
>     day01. MySQL 高级 - 索引 - 数据结构 - BTREE
>     day01. MySQL 高级 - 索引 - 数据结构 - B+TREE
>     day01. MySQL 高级 - 索引 - 索引分类
>     day01. MySQL 高级 - 索引 - 索引语法
>     day01. MySQL 高级 - 索引 - 索引设计原则
>     day01. MySQL 高级 - 视图 - 概述
>     day01. MySQL 高级 - 视图 - 创建及修改视图
>     day01. MySQL 高级 - 视图 - 查看及删除视图
>     day01. MySQL 高级 - 存储过程 - 概述
>     day01. MySQL 高级 - 存储过程 - 创建调用查询删除语法
>     day01. MySQL 高级 - 存储过程 - 语法 - 变量
>     day01. MySQL 高级 - 存储过程 - 语法 - if判断
>     day01. MySQL 高级 - 存储过程 - 语法 - 输入参数
>     day01. MySQL 高级 - 存储过程 - 语法 - 输出参数
>     day01. MySQL 高级 - 存储过程 - 语法 - case结构
>     day01. MySQL 高级 - 存储过程 - 语法 - while循环
>     day01. MySQL 高级 - 存储过程 - 语法 - repeat循环
>     day01. MySQL 高级 - 存储过程 - 语法 - loop循环
>     day01. MySQL 高级 - 存储过程 - 语法 - 游标介绍
>     day01. MySQL 高级 - 存储过程 - 语法 - 游标基本操作
>     day01. MySQL 高级 - 存储过程 - 语法 - 循环获取游标
>     day01. MySQL 高级 - 存储过程 - 函数
>     day01. MySQL 高级 - 触发器 - 介绍
>     day01. MySQL 高级 - 触发器 - 创建及应用
>     day01. MySQL 高级 - 触发器 - 查看及删除
>     day02. MySQL高级 - 体系结构
>     day02. MySQL高级 - 存储引擎 - 概述
>     day02. MySQL高级 - 存储引擎 - 特性
>     day02. MySQL高级 - 存储引擎 - InnoDB特性
>     day02. MySQL高级 - 存储引擎 - MyISAM特性
>     day02. MySQL高级 - 存储引擎 - Memory与Merge特性
>     day02. MySQL高级 - 存储引擎 - 选择原则
>     day02. MySQL高级 - 优化SQL步骤 - SQL执行频率
>     day02. MySQL高级 - 优化SQL步骤 - 定位低效SQL
>     day02. MySQL高级 - 优化SQL步骤 - explain指令介绍
>     day02. MySQL高级 - 优化SQL步骤 - explain之id
>     day02. MySQL高级 - 优化SQL步骤 - explain之select_type
>     day02. MySQL高级 - 优化SQL步骤 - explain之table type
>     day02. MySQL高级 - 优化SQL步骤 - explain之key rows extra
>     day02. MySQL高级 - 优化SQL步骤 - show profile
>     day02. MySQL高级 - 优化SQL步骤 - trace工具
>     day02. MySQL高级 - 索引的使用 - 验证索引提升查询效率
>     day02. MySQL高级 - 索引的使用 - 全值匹配
>     day02. MySQL高级 - 索引的使用 - 最左前缀法则
>     day02. MySQL高级 - 索引的使用 - 索引失效情况(范围查询，字段运算)
>     day02. MySQL高级 - 索引的使用 - 覆盖索引
>     day02. MySQL高级 - 索引的使用 - or索引失效情况
>     day02. MySQL高级 - 索引的使用 - like模糊匹配
>     day02. MySQL高级 - 索引的使用 - 全表扫描更快
>     day02. MySQL高级 - 索引的使用 - NULL值的判定
>     day02. MySQL高级 - 索引的使用 - in和not in
>     day02. MySQL高级 - 索引的使用 - 单列索引与复合索引选择
>     day02. MySQL高级 - 索引的使用 - 查看索引使用情况
>     day02. MySQL高级 - SQL优化 - 大批量插入数据
>     day02. MySQL高级 - SQL优化 - insert优化
>     day02. MySQL高级 - SQL优化 - orderby 优化
>     day02. MySQL高级 - SQL优化 - group by 优化
>     day02. MySQL高级 - SQL优化 - 子查询优化
>     day02. MySQL高级 - SQL优化 - or优化
>     day02. MySQL高级 - SQL优化 - limit优化
>     day02. MySQL高级 - SQL优化 - 索引提示
>     day03. MySQL高级 - 应用优化
>     day03. MySQL高级 - 查询缓存 - 概述及流程
>     day03. MySQL高级 - 查询缓存 - 配置参数
>     day03. MySQL高级 - 查询缓存 - 开启查询缓存
>     day03. MySQL高级 - 查询缓存 - SELECT选项
>     day03. MySQL高级 - 查询缓存 - 失效场景
>     day03. MySQL高级 - 内存优化 - 优化原则
>     day03. MySQL高级 - 内存优化 - MyISAM内存优化
>     day03. MySQL高级 - 内存优化 - InnoDB内存优化
>     day03. MySQL高级 - 并发参数调整
>     day03. MySQL高级 - 锁 - 锁的概述及分类
>     day03. MySQL高级 - 锁 - MySQL对锁的支持
>     day03. MySQL高级 - 锁 - MyISAM表锁 - 读锁
>     day03. MySQL高级 - 锁 - MyISAM表锁 - 写锁
>     day03. MySQL高级 - 锁 - MyISAM表锁 - 小结
>     day03. MySQL高级 - 锁 - MyISAM表锁 - 查看锁争用情况
>     day03. MySQL高级 - 锁 - InnoDB行锁 - 介绍及背景知识
>     day03. MySQL高级 - 锁 - InnoDB行锁 - 基本演示
>     day03. MySQL高级 - 锁 - InnoDB行锁 - 行锁升级为表锁
>     day03. MySQL高级 - 锁 - InnoDB行锁 - 间隙锁危害
>     day03. MySQL高级 - 锁 - InnoDB行锁 - 争用情况查看
>     day03. MySQL高级 - 锁 - InnoDB行锁 - 总结
>     day03. MySQL高级 - SQL技巧 - SQL执行顺序及正则表达式
>     day03. MySQL高级 - SQL技巧 - 数字函数与字符串函数
>     day03. MySQL高级 - SQL技巧 -日期函数与聚合函数
>     day04. MySQL高级 - 常用工具 - mysql
>     day04. MySQL高级 - 常用工具 - mysqladmin
>     day04. MySQL高级 - 常用工具 - mysqlbinlog与mysqldump
>     day04. MySQL高级 - 常用工具 - mysqlimport与source
>     day04. MySQL高级 - 常用工具 - mysqlshow
>     day04. MySQL高级 - 日志 - 错误日志
>     day04. MySQL高级 - 日志 - 二进制日志(statement)
>     day04. MySQL高级 - 日志 - 二进制日志(row及日志删除)
>     day04. MySQL高级 - 日志 - 查询日志
>     day04. MySQL高级 - 日志 - 慢查询日志
>     day04. MySQL高级 - 复制 - 原理
>     day04. MySQL高级 - 复制 - 集群搭建
>     day04. MySQL高级 - 案例 - 需求及环境准备
>     day04. MySQL高级 - 案例 - 基本工程导入
>     day04. MySQL高级 - 案例 - AOP记录日志
>     day04. MySQL高级 - 案例 - 日志查询后端 - mapper接口
>     day04. MySQL高级 - 案例 - 日志查询后端 - Service&Controller
>     day04. MySQL高级 - 案例 - 日志查询 - 前端
>     day04. MySQL高级 - 案例 - 系统性能优化分析
>     day04. MySQL高级 - 案例 - 系统性能优化 - 分页优化
>     day04. MySQL高级 - 案例 - 系统性能优化 - 索引优化
>     day04. MySQL高级 - 案例 - 系统性能优化 - 读写分离概述
>     day04. MySQL高级 - 案例 - 系统性能优化 - 数据源配置
>     day04. MySQL高级 - 案例 - 系统性能优化 - AOP切换数据源
>     day04. MySQL高级 - 案例 - 系统性能优化 - AOP切换数据源 - 测试
>     day04. MySQL高级 - 案例 - 系统性能优化 - AOP切换数据源 - 原理解析
>     day04. MySQL高级 - 案例 - 系统性能优化 - 应用优化
